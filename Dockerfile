# Use a smaller Node.js base image
FROM node:alpine

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm install && \
    npm cache clean --force

# Copy other project files
COPY . .

# Expose port 3000 (or whichever port your application uses)
EXPOSE 3000

# Command to run the application
CMD ["npm", "start"]
