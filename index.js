const express = require('express');
const app = express();
const path = require('path');

// Set the view engine to ejs
app.set('view engine', 'ejs');

// Serve static files
app.use(express.static(path.join(__dirname, 'Public')));

// Home route
app.get('/', (req, res) => {
    res.render('index');
});

// Listings route
app.get('/listings', (req, res) => {
    const listings = [
        { id: 1, type: 'Semi-Furnished', location: 'Janki nagar colony', price: 1500, image: '/images/house1.jpg' },
        { id: 2, type: 'Fully Furnished', location: 'Nizam colony', price: 2500, image: '/images/house2.jpg' },
        { id: 3, type: 'Semi-Furnished', location: 'Gulshan colony', price: 1200, image: '/images/house3.jpg' },
        { id: 4, type: 'Fully Furnished', location: 'Paramount colony', price: 3000, image: '/images/house4.jpg' }
    ];
    res.render('listings', { listings });
});

// Contact route
app.get('/contact', (req, res) => {
    res.render('contact');
});

// Start the server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

